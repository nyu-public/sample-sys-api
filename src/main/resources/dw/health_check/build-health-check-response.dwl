%dw 1.0
%output application/json
---
{
  "status": flowVars.overall_status,
  "worker ip": flowVars.worker_ip,
  "dependencies": [
    {
		"name": "MySQL DB",
		"code": "mysql-db",
		"description": "MYSQL DB",
		"status": flowVars.systemsHealthStatus.mysql
    }
  ]
}